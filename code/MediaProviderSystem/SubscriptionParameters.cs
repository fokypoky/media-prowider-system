namespace MediaProviderSystem;

public enum SubscriptionParameters
{
    Default,
    WithDiscount
}