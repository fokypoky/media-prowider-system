﻿using System;
using System.Runtime.InteropServices;

namespace MediaProviderSystem;

class Program
{
    public static void Main()
    {
        var tvController = new TvSubscriptionController();
        var defaultTvSubscription = tvController.GetSubscription(SubscriptionParameters.Default);
        var withDiscountTvSubscription = tvController.GetSubscription(SubscriptionParameters.WithDiscount);

        var intController = new InternetSubscriptionController();
        var defaultIntSub = intController.GetSubscription(SubscriptionParameters.Default);
        var withDiscountIntSub = intController.GetSubscription(SubscriptionParameters.WithDiscount);

        Console.WriteLine(defaultTvSubscription?.GetInfo());
        Console.WriteLine(withDiscountTvSubscription?.GetInfo());
        Console.WriteLine(defaultIntSub?.GetInfo());
        Console.WriteLine(withDiscountIntSub?.GetInfo());
        
    }
}