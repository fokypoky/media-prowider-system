using System.Runtime.CompilerServices;

namespace MediaProviderSystem;

public class WithDiscountSubscriptionStrategy : SubscriptionStrategy
{
    private int _discountPercent = 10;
    public WithDiscountSubscriptionStrategy()
    {
    }

    public override decimal GetCost() => _cost - (_cost / 100 * _discountPercent);
}