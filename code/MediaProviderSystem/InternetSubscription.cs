namespace MediaProviderSystem;

public class InternetSubscription : IMediaSubscription
{
    public SubscriptionStrategy Strategy { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public decimal Cost { get; set; }
    public DateTime EndDate { get; set; }

    public InternetSubscription(SubscriptionStrategy strategy)
    {
        Strategy = strategy;
        Title = "INTERNET";
        Description = "Home Internet package";
        Cost = strategy.GetCost();
        EndDate = DateTime.Now.AddMonths(1);
    }
    public void Extend(int monthCount) => EndDate = EndDate.AddMonths(monthCount);
    public string GetInfo() => $"{Title} - {Description} - {Cost} - {EndDate}";
}