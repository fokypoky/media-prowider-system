namespace MediaProviderSystem;

public abstract class SubscriptionFactory
{
    public SubscriptionFactory()
    {
    }

    public abstract IMediaSubscription? MakeSubscription(SubscriptionParameters subscriptionParameters);
}