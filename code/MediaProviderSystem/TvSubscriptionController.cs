﻿namespace MediaProviderSystem;

public class TvSubscriptionController
{
    public TvSubscription? GetSubscription(SubscriptionParameters parameters) =>
        (TvSubscription) new MediaProviderFacade<TvSubscriptionFactory>().CreateSubscription(parameters);
}