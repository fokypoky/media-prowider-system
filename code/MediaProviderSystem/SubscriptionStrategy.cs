namespace MediaProviderSystem;

public abstract class SubscriptionStrategy
{
    protected decimal _cost = 1000;

    public SubscriptionStrategy()
    {
    }

    public abstract decimal GetCost();
}