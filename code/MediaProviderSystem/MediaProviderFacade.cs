namespace MediaProviderSystem;

public class MediaProviderFacade<T> where T : SubscriptionFactory, new()
{
    private T _factory;

    public MediaProviderFacade()
    {
        _factory = new();
    }

    public IMediaSubscription? CreateSubscription(SubscriptionParameters parameters) =>
        _factory.MakeSubscription(parameters);

    public void ExpandSubscription(IMediaSubscription subscription, int monthCount) => subscription.Extend(monthCount);
}