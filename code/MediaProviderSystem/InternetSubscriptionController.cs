﻿namespace MediaProviderSystem;

public class InternetSubscriptionController
{
    public InternetSubscription? GetSubscription(SubscriptionParameters parameters) =>
        (InternetSubscription) new MediaProviderFacade<InternetSubscriptionFactory>().CreateSubscription(parameters);
}