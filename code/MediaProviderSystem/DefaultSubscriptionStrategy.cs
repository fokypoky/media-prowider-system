namespace MediaProviderSystem;

public class DefaultSubscriptionStrategy : SubscriptionStrategy
{
    public override decimal GetCost() => _cost;
}