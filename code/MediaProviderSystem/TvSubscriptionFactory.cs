namespace MediaProviderSystem;

public class TvSubscriptionFactory : SubscriptionFactory
{
    public override IMediaSubscription? MakeSubscription(SubscriptionParameters subscriptionParameters)
    {
        switch (subscriptionParameters)
        {
            case SubscriptionParameters.Default:
                return new TvSubscription(new DefaultSubscriptionStrategy());
            case SubscriptionParameters.WithDiscount:
                return new TvSubscription(new WithDiscountSubscriptionStrategy());
            default:
                return null;
        }
    }
}