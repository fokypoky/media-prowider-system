namespace MediaProviderSystem;

public class InternetSubscriptionFactory : SubscriptionFactory
{
    public override IMediaSubscription? MakeSubscription(SubscriptionParameters parameters)
    {
        switch (parameters)
        {
            case SubscriptionParameters.Default:
                return new InternetSubscription(new DefaultSubscriptionStrategy());
            case SubscriptionParameters.WithDiscount:
                return new InternetSubscription(new WithDiscountSubscriptionStrategy());
            default:
                return null;
        }
    }
}